import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

interface TheRocket {
    int step_size = 5;
    void left();
    void right();
    void fire();
    TheEnemy check_colision();
    void check_keys();
    void explode();
}


public class MyRocket extends Actor implements TheRocket {
    TheJoystick joystick = new MyJoystick();
    TheBullet bullet = new MyBullet();
    TheExplosion explosion = new MyExplosion();

    public MyRocket() {
        explosion.init(this);
        setRotation(270);
    }
    
    public void move(int size) {
        int x = getX() + size;
        int y = getY();
        setLocation(x, y);
    }

    public void check_keys() {
        joystick.check_keys();
        if( joystick.left() ) left();
        if( joystick.right() ) right();
        if( joystick.fire() ) fire();
    }

    public void act() {
        check_keys();
        if( check_colision() != null )
            explode();
    }
    
    public void left() {
        move(-step_size);
    }

    public void right() {
        move(step_size);
    }

    public void fire() {
        if( bullet.is_running() ) return;

        TheGame game = getWorldOfType(TheGame.class);
        game.add(bullet, getX(), getY());
        bullet.start();
    }

    public TheEnemy check_colision() {
        List<TheEnemy> enemies = getIntersectingObjects(TheEnemy.class);
        if( enemies.size() <= 0 ) return null;

        TheEnemy enemy = enemies.get(0);
        return enemy;
    }

    public void explode() {
        TheGame game = getWorldOfType(TheGame.class);
        int x = getX();
        int y = getY();

        game.add(explosion, x, y);
        game.remove(this);
        explosion.start(2);
    }
    
}

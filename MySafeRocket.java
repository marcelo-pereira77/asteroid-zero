import greenfoot.*;

interface TheSafeRocket extends TheRocket {
    int safe_time = 5;  // seconds
    void check_timeout();
    void start_safe(int seconds);
    void stop_safe();
    boolean is_safe();
}


public class MySafeRocket extends MyRocket implements TheSafeRocket {
    long _safe_timeout = -1;

    protected void addedToWorld​(World world){
        start_safe(safe_time);
    }

    public void start_safe(int seconds) {
        _safe_timeout = System.currentTimeMillis() + (seconds * 1000);
        GreenfootImage img = getImage();
        img.setTransparency(80);
    }

    public void stop_safe() {
        _safe_timeout = -1;
        GreenfootImage img = getImage();
        img.setTransparency(255);
    }
    
    public boolean is_safe() {
        return _safe_timeout > 0;
    }
    
    public void check_timeout() {
        if( ! is_safe() ) return;
        
        long now = System.currentTimeMillis();
        if( now < _safe_timeout ) return;

        stop_safe();
    }

    public void act() {
        super.act();
        check_timeout();
    }
    
    public void fire() {
        if( is_safe() ) return;

        super.fire();
    }

    public void explode() {
        if( is_safe() ) return;

        super.explode();
    }
    
}

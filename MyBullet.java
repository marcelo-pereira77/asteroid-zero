import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

interface TheBullet {
    void start();
    void stop();
    boolean is_running();
    TheEnemy check_colision();
}

public class MyBullet extends Actor implements TheBullet {
    boolean _running = false;
    
    public MyBullet() {
        setRotation(270);
    }
    
    public void start() {
        _running = true;
    }

    public void stop() {
        _running = false;
        TheGame game = getWorldOfType(TheGame.class);
        if( game != null )
            game.remove(this);
    }

    public boolean is_running() {
        return _running;
    }
    
    public TheEnemy check_colision() {
        TheGame game = getWorldOfType(TheGame.class);
        if( game == null ) {
            _running = false;
            return null;
        }

        List<TheEnemy> enemies = getIntersectingObjects(TheEnemy.class);
        if( enemies.size() <= 0 ) return null;
        TheEnemy enemy = enemies.get(0);
        return enemy;
    }
    
    public void act() {
        move(5);
        if( isAtEdge() ) stop();

        TheEnemy enemy = check_colision();
        if( enemy != null ) {
            enemy.break_up();
            stop();
        }
    }
}

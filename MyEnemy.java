import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

interface TheEnemy {
    int init_velocity = 1;
    int init_size = 50;
    
    void break_up();
    void init(int size, int velocity);
    void rand_direction();
    int check_margin(int value, int min, int max);
    void move(int distance);
}

public class MyEnemy extends MyMover implements TheEnemy {
    int size;
    int velocity;

    public MyEnemy() {
        this(init_size, init_velocity);
    }

    public MyEnemy(int size, int velocity) {
        init(size, velocity);
    }
    
    public void init(int size, int velocity) {
        GreenfootImage image = getImage();
        image.scale(size, size);
        this.size = size;
        this.velocity = velocity;
        rand_direction();
    }
    
    public void rand_direction() {
        int rotation = 0;
        while(rotation < 5 || (rotation>175 && rotation<185))
            rotation = Greenfoot.getRandomNumber(375);
        setRotation(rotation);
    }
    
    public void break_up() {
        TheGame game = getWorldOfType(TheGame.class);

        int x = getX();
        int y = getY();
        game.remove(this);

        if( size <= 15 ) return;

        int newsize = size/2;
        for(int i=0; i<3; i++) {
            TheEnemy enemy = new MyEnemy(newsize, velocity);
            game.add(enemy, x, y);
        }

    }
    
    
    public void act()
    {
        move(velocity);
    }
}

import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

interface TheExplosion {
    void init(TheRocket r);
    void check_timeout();
    void start(int seconds);
    void stop();
}

public class MyExplosion extends Actor implements TheExplosion {
    TheRocket _rocket = null;
    long timeout = -1;
    
    public void init(TheRocket r) {
        _rocket = r;
    }
    
    public void start(int seconds) {
        timeout = System.currentTimeMillis() + (seconds * 1000);
    }

    public void stop() {
        timeout = -1;
        TheGame game = getWorldOfType(TheGame.class);
        game.add(_rocket, getX(), getY());
        game.remove(this);
    }
    
    
    public void act() {
        check_timeout();
    }
    
    public void check_timeout() {
        if( timeout <= 0 ) return;

        long now = System.currentTimeMillis();
        if( now < timeout ) return;
        
        stop();
    }
}

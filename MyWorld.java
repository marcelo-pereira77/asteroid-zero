import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

interface TheGame {
    int width = 600;
    int height = 400;

    void init_game();
    void finish_game();
    void start_game();
    
    void init_background();
    void init_rocket();
    void init_enemies();
    void init_scoreboard();

    void add(TheEnemy enemy, int x, int y);
    void add(TheBullet bullet, int x, int y);
    void add(TheRocket rocket, int x, int y);
    void add(TheExplosion explosion, int x, int y);
    void add(TheScoreboard score, int x , int y);

    void remove(TheEnemy enemy);
    void remove(TheBullet bullet);
    void remove(TheRocket rocket);
    void remove(TheExplosion explosion);
}


public class MyWorld extends World implements TheGame
{
    boolean end_of_game = false;
    TheScoreboard score;
    
    public MyWorld() {    
        super(600, 400, 1); 
        init_game();
    }
    
    public void init_game() {
        // register timer, or start here..
        start_game();
    }

    public void finish_game() {
        end_of_game = true;
    }
    
    public void act() {
        if(end_of_game)
            Greenfoot.stop();
    }
    
    
    public void start_game() {
        init_background();
        init_rocket();
        init_enemies();
        init_scoreboard();
    }
    
    public void init_background() {
        GreenfootImage background = getBackground();
        background.setColor(Color.BLACK);
        background.fill();

        for(int i=0; i<300; i++) {
            int w = Greenfoot.getRandomNumber(TheGame.width);
            int h = Greenfoot.getRandomNumber(TheGame.height);

            int value = Greenfoot.getRandomNumber(255);
            
            Color color = new Color(value, value, value);
            background.setColor(color);
            int size = Greenfoot.getRandomNumber(3) + 1; // 1 to 4
            background.fillOval(w, h, size, size);
        }
    }
    
    
    public void init_rocket() {
        TheRocket rocket = new MyRocket();
        add(rocket, 300, 350);
    }
    
    public void init_enemies() {
        TheEnemy enemy = new MyEnemy();
        add(enemy, 100, 100);

        enemy = new MyEnemy();
        add(enemy, 100, 120);    

        enemy = new MyEnemy();
        add(enemy, 100, 140);}

    public void init_scoreboard() {
        score = new MyScoreboard();
        add(score, 550, 15);
    }

    
    // ------ ENEMY -------
    public void add(TheEnemy enemy, int x, int y) {
        Actor myactor = (Actor)enemy;
        super.addObject(myactor, x, y);
    }

    public void remove(TheEnemy enemy) {
        Actor myactor = (Actor)enemy;
        super.removeObject(myactor);
        score.add(15);
    }
    
    // ------ BULLET -------
    public void add(TheBullet bullet, int x, int y) {
        Actor myactor = (Actor)bullet;
        super.addObject(myactor, x, y);
    }

    public void remove(TheBullet bullet) {
        Actor myactor = (Actor)bullet;
        super.removeObject(myactor);
    }
    
    // ------ ROCKET -------
    public void add(TheRocket rocket, int x, int y) {
        Actor myactor = (Actor)rocket;
        super.addObject(myactor, x, y);
    }
    
    public void remove(TheRocket rocket) {
        Actor myactor = (Actor)rocket;
        super.removeObject(myactor);
    }


    // ------ EXPLOSION -------
    public void add(TheExplosion explosion, int x, int y) {
        Actor myactor = (Actor)explosion;
        super.addObject(myactor, x, y);
    }
    
    public void remove(TheExplosion explosion) {
        Actor myactor = (Actor)explosion;
        super.removeObject(myactor);
    }

    
    // -------- SCOREBOARD -------------
    public void add(TheScoreboard score, int x , int y) {
        Actor myactor = (Actor) score;
        super.addObject(myactor, x, y);
    }

    
    
}

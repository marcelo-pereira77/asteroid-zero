import greenfoot.*;


public abstract class MyMover extends Actor
{
    double exactX;
    double exactY;

    public void move(int dist)
    {
        double distance = dist;

        double radians = Math.toRadians(getRotation());
        double dx = Math.cos(radians) * distance;
        double dy = Math.sin(radians) * distance;

        setLocation(exactX+dx, exactY+dy);
    }
    
    public void setLocation(double x, double y) {
        exactX = check_margin(x, 10, TheGame.width-10);
        exactY = check_margin(y, 10, TheGame.height-10);

        int newx = (int)(exactX+0.5);
        int newy = (int)(exactY+0.5);
        
        super.setLocation(newx, newy);
    }
    
    public void setLocation(int x, int y) {
        x = check_margin(x, 0+10, TheGame.width);
        y = check_margin(y, 10, TheGame.height-10);

        exactX = x;
        exactY = y;
        super.setLocation(x, y);
    }

    public int check_margin(int value, int min, int max) {
        if(value < min) value = max;
        if(value > max) value = min;
        return value;
    }

    public double check_margin(double value, int min, int max) {
        if(value < min) value = max;
        if(value > max) value = min;
        return value;
    }




}

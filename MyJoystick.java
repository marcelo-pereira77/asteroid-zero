import greenfoot.Greenfoot;

interface TheJoystick {
    boolean left();
    boolean right();
    boolean fire();
    void check_keys();
}


public class MyJoystick implements TheJoystick {
    boolean _left=false, _right=false, _fire = false;
    
    public boolean left() {
        boolean ret = _left;
        _left = false;
        return ret;
    }
    
    public boolean right() {
        boolean ret = _right;
        _right = false;
        return ret;
    }
    
    public boolean fire() {
        boolean ret = _fire;
        _fire = false;
        return ret;
    }
    
    public void check_keys() {
        if(Greenfoot.isKeyDown("space")) {
            _fire = true;
            _left = _right = false;
            return;
        }

        if(Greenfoot.isKeyDown("left")) {
            _left=true;
            _right = _fire = false;
            return;
        }
        
        if(Greenfoot.isKeyDown("right")) {
            _right= true;
            _left = _fire = false;
            return;
        }
        
    }
}
